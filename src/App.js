import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          CI/CD Test
        </p>
      </header>
    </div>
  );
}

export default App;
