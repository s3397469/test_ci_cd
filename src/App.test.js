import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('main test', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/CD Test/i);
  expect(linkElement).toBeInTheDocument();
});
